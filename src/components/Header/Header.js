import React from 'react';

import Search from './../Search/Search';
import { Wrapper, SearchWrapper } from './Header.style';

export default function Header() {
  return (
    <Wrapper>
      <SearchWrapper>
        <Search />
      </SearchWrapper>
    </Wrapper>
  );
}
