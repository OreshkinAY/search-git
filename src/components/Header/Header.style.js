import styled from 'styled-components';

export const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  height: 70px;
`;

export const SearchWrapper = styled.div`
  display: flex;
  align-items: center;
`;