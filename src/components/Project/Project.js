// @flow
import React from 'react';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

import { useStyles } from './Project.style';
import type { Project } from './../../types/project';

type Props = {
  children: string,
  ...Project,
};

export default function SimpleCard(props: Props) {
  const { html_url, stargazers_count, watchers_count } = props;
  const classes = useStyles();
  const seeProject = () => {
    window.location = html_url;
  }

  return (
    <Card className={classes.card}>
      <CardContent>
        <Typography onClick={seeProject} className={`${classes.pos} ${classes.title}`} >
          {props.children}
        </Typography>
        <Typography color='textSecondary'>
          Количество звезд: {stargazers_count}
        </Typography>
        <Typography color='textSecondary'>
          Kоличество подписчиков: {watchers_count}
        </Typography>
      </CardContent>
      <CardActions>
        <Button onClick={seeProject} size='small'>К проекту</Button>
      </CardActions>
    </Card>
  );
}
