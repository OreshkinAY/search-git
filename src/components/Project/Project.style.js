import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles({
  card: {
    height: '100%',
    display: 'flex',
    justifyContent: 'space-between',
    flexDirection: 'column'
  },
  title: {
    fontSize: 14,
    cursor: 'pointer',
    '&:hover': {
      backgroundColor: 'rgba(0, 0, 0, 0.08)',
    },
  },
  pos: {
    marginBottom: 12
  }
});