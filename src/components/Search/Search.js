// @flow
import React from 'react';
import { connect } from 'react-redux';
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import SearchIcon from '@material-ui/icons/Search';

import * as searchActions from '../../redux/actions/projects';
import { useStyles } from './Search.style';

type Props = {
  search: (e: string) => void
};

const Search = (props: Props) => {
  const classes = useStyles();

  const searchRequest = (e) => {
    const { search } = props;
    search(e.target.value)
  }

  return (
    <Paper className={classes.root}>
      <IconButton className={classes.iconButton} aria-label='Menu'>
        <MenuIcon />
      </IconButton>
      <InputBase
        className={classes.input}
        placeholder='Поиск'
        inputProps={{ 'aria-label': 'Поиск' }}
        onChange={searchRequest}
      />
      <IconButton className={classes.iconButton} aria-label='Search'>
        <SearchIcon />
      </IconButton>
    </Paper>
  );
}

const mapDispatch = dispatch => ({
  search: searchText => dispatch(searchActions.searchProject(searchText)),
});

export default connect(null, mapDispatch)(Search);

