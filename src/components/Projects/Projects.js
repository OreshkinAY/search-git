// @flow
import * as React from 'react';
import { connect } from 'react-redux';
import Grid from '@material-ui/core/Grid';

import * as selectors from './../../redux/selectors/project.selectors';
import { Wrapper } from './Projects.style';
import Project from './../Project/Project';
import type { ProjectsType } from './../../types/project';

type Props = {
  projects: ProjectsType
};

const Projects = ({ projects }: Props) => (
  <Wrapper>
    {projects.items.length !== 0 &&
      <Grid
        container
        direction='row'
        justify='center'
        alignItems='stretch'
        spacing={3}
      >
        {projects.items.map(({ id, name, ...othersProps }) => (
          <Grid key={id} item sm={4} >
            <Project  {...othersProps}>
              {name}
            </Project>
          </Grid>
        ))}
      </Grid>}
    {projects.total_count === 0 && <div>Ничего не найдено</div>}
  </Wrapper>
);

const mapState = state => ({ projects: selectors.loaded(state) });

export default connect(mapState)(Projects);
