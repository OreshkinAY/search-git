import { call } from 'redux-saga/effects';
import axios from 'axios';

export function* request(payload) {
  let success;
  let error;

  try {
    success = yield call(axios, payload);
  } catch (e) {
    error = e;
  }

  return [success, error];
}
