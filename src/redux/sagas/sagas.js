import { call, debounce, put } from 'redux-saga/effects';
import * as actions from '../actions/projects';
import Api from '../../Api/Api';

function* searchProjectSaga(action) {
    let { payload } = action;
    const requestPayload = {
        url: `https://api.github.com/search/repositories?q=${payload}`,
        method: 'get',
    };

    const [success, error] = yield call(Api.request, requestPayload);
    if (success) {
        yield put(actions.loaded(success.data));
    }
    if (error) {
        console.error(error);
    }
}

export default function* watch() {
    yield debounce(1000, actions.searchProject, searchProjectSaga)
}