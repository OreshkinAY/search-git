// @flow
import * as actions from '../actions/projects';
import { handleActions } from 'redux-actions';

import type { ProjectsType } from './../../types/project';

const initState: ProjectsType = {
  items: [],
  total_count: null,
};

const projects = handleActions({
  [actions.loaded](state, { payload: { items, total_count } }) {
    return { items, total_count };
  },
}, initState);

export { projects };