
import { createAction } from 'redux-actions';

export const searchProject = createAction('SEARCH_PROJECT');
export const loaded = createAction('LOADED_PROJECT');
