import React, { Component } from 'react';
import { Provider } from 'react-redux';

import { store } from './redux/store/configureStore';
import Header from './components/Header/Header';
import Projects from './components/Projects/Projects';

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Header />
        <Projects />
      </Provider>
    );
  }
}

export default App;
